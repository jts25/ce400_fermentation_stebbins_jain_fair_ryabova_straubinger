import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
#This is a test2

def f(C_bacteria, t, m_lactic_acid, params):
        # unpack current values of y
    C_glucose, C_lactic_acid, V = params  # unpack parameters
    derivs = [  # list of dy/dt=f functions
              (.54*C_glucose/(100.0+C_glucose))*C_bacteria*((1.0-C_lactic_acid)/100.0)]
    return derivs

# Parameters
V = 1         # forcing amplitude
C_glucose = m_glucose/V
C_lactic_acid = m_lactic_acid/V

# Initial values
C_bacteria0 = 0.0     # initial angular displacement
t0 = 0.0     # initial time
m_lactic_acid0 = 0.0

# Bundle parameters for ODE solver
params = [C_glucose, C_lactic_acid, V]

# Bundle initial conditions for ODE solver
y0 = [C_bacteria0, t0, m_lactic_acid0]

# Make time array for solution
tStop = 200.
tInc = 0.05
t = np.arange(0., tStop, tInc)

# Call the ODE solver
psoln = odeint(f, y0, t, args=(params,))

# Plot results
fig = plt.figure(1, figsize=(8,8))

# Plot theta as a function of time
ax1 = fig.add_subplot(311)
ax1.plot(t, psoln[:,0])
ax1.set_xlabel('time')
ax1.set_ylabel('theta')

# Plot omega as a function of time
ax2 = fig.add_subplot(312)
ax2.plot(t, psoln[:,1])
ax2.set_xlabel('time')
ax2.set_ylabel('omega')

# Plot omega vs theta
ax3 = fig.add_subplot(313)
twopi = 2.0*np.pi
ax3.plot(psoln[:,0]%twopi, psoln[:,1], '.', ms=1)
ax3.set_xlabel('theta')
ax3.set_ylabel('omega')
ax3.set_xlim(0., twopi)

plt.tight_layout()
plt.show()